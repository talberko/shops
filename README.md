## Start the server

1. Install dependencies by running ```npm install```
2. Start the server by running ```npm start```

---

## API 

#### Get shop by id 
```GET http://localhost:3000/shops/{id}```

Accepts an id and returns the id, name, address, latitude, and longitude of the coffee shop with that id, or an appropriate error if it is not found.

---

#### Create shop 
```POST http://localhost:3000/shops```

Adds a new coffee shop to the data set, and returns the id of the new coffee shop. <br>
Accepts ```name```, ```address```, ```latitude```, and ```longitude``` body params.

---  

#### Update shop 
```PATCH http://localhost:3000/shops/{id}```  

Updates the coffee shop with that id, or returns an appropriate error if it is not found. <br>
Accepts ```name```, ```address```, ```latitude```, and ```longitude``` body params.

---  
#### Delete shop 
```DELETE http://localhost:3000/shops/{id}```  

Accepts an id and deletes the coffee shop with that id, or returns an error if it is not found <br>

---  
#### Find nearest shop 

```GET http://localhost:3000/shops/nearest?address={address}```

Accepts an address and returns the closest coffee shop by straight line distance.
 
---  