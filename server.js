const {init} = require("./services/locations")
const bodyParser = require('body-parser');

const express = require('express')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

init().then(() => {
	app.use('/shops', require('./routes/shops'))

	app.listen(3000, () => console.log("Started"))
}).catch(() => console.log("Cannot initialize locations"))
