const router = require('express').Router()
const {locations} = require('../services/locations')
const _ = require('lodash')
const haversine = require('haversine')
const {check, validationResult} = require('express-validator/check');

const googleMapsClient = require('@google/maps').createClient({
	key: 'AIzaSyDtU6dBbbwdz5Rqr_g2MOUxHz-mojwp_e8',
	Promise: Promise
});

// define the about route
router.post('/', [
	check('name').not().isEmpty().isString().trim(),
	check('address').exists().isString().trim(),
	check('longitude').exists().isDecimal().trim(),
	check('latitude').exists().isDecimal().trim(),
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({errors: errors.array()});
	}

	const {name, address, longitude, latitude} = req.body

	const id = _.maxBy(locations, 'id')

	locations.push({
		id,
		name,
		address,
		longitude,
		latitude
	})

	res.send(id)
})

router.patch('/:id', [
	check('name').optional().not().isEmpty().isString().trim(),
	check('address').optional().exists().isString().trim(),
	check('longitude').optional().exists().isDecimal().trim(),
	check('latitude').optional().exists().isDecimal().trim(),
], (req, res) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(422).json({errors: errors.array()});
	}

	const {name, address, longitude, latitude} = req.body

	const idx = locations.findIndex(l => l.id === req.params.id)

	if (idx === -1)
		return res.sendStatus(404)

	const location = locations[idx]

	locations[idx] = {
		id: location.id,
		name: name || location.name,
		address: address || location.address,
		longitude: longitude || location.longitude,
		latitude: latitude || location.latitude
	}

	res.sendStatus(204)
})

router.delete('/:id', (req, res) => {
	const idx = locations.findIndex(l => l.id === req.params.id)

	if (idx === -1)
		return res.sendStatus(404)

	locations.splice(idx, 1)
	res.sendStatus(204)
})

router.get('/nearest', async (req, res) => {
	const {address} = req.query

	try {
		const geolocation = await googleMapsClient.geocode({address})
			.asPromise().then(res => res.json.results)

		if (geolocation.length === 0)
			return res.sendStatus(404)

		const {lat, lng} = geolocation[0].geometry.location

		const min = _.minBy(locations, (l) => haversine({latitude: l.latitude, longitude: l.longitude}, {
			latitude: lat,
			longitude: lng
		}))

		res.json(min)
	}
	catch (e) {
		res.sendStatus(500)
	}
})

router.get('/:id', (req, res) => {
	const shop = locations.find(l => l.id === req.params.id)
	console.log(shop)
	if (!shop)
		return res.sendStatus(404)

	return res.json(shop)
})

module.exports = router