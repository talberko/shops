const csv = require('csvtojson')
const path = require('path')

let locations = []

const init = async () => {
	const parsedLocations = await csv({
		noheader: true,
		headers: ['id', 'name', 'address', 'latitude', 'longitude']
	}).fromFile(path.join(__dirname, '../data/locations.csv'))

	locations.push(...parsedLocations)
}

module.exports = {
	locations,
	init
}